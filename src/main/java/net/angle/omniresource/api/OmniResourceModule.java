package net.angle.omniresource.api;

import net.angle.omnimodule.api.OmniModule;
import pixelguys.json.JsonObject;

/**
 *
 * @author angle
 */
public interface OmniResourceModule extends OmniModule {
    public ResourceManager getPrivateResourceManager();
    public ResourceManager getSharedResourceManager(String name);
    
    public ResourceType<Resource<JsonObject>> getJSONResourceType();
}