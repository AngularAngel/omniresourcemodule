package net.angle.omniresource.api;

/**
 *
 * @author angle
 */
public interface ResourceType<T extends Resource> {
    public String[] getExtensions();
    public void setExtensions(String[] extensions);
    
    public T getResource(String name);
}