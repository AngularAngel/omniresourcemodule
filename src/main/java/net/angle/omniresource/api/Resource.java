/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniresource.api;

import net.angle.omniresource.impl.BasicResourceType;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author angle
 */
public interface Resource<T> {
    
    public String getName();
    public default void load(ResourceManager resourceManager) throws IOException {
        load(resourceManager, this.getClass());
    }
    public void load(ResourceManager resourceManager, Class clazz) throws IOException;
    public void unload(ResourceManager resourceManager);
    public boolean isLoaded();
    public String getResourcePath();
    public T get();
    public ResourceType<? extends Resource<T>> getType();
    public default InputStream getInputStream() throws IOException {
        return getInputStream(this.getClass());
    }
    public default InputStream getInputStream(Class clazz) throws IOException {
        try {
            return new FileInputStream(getResourcePath());
        } catch (IOException exception) {
            InputStream resourceAsStream = clazz.getResourceAsStream("/" + getResourcePath());
            if (resourceAsStream == null) throw new IOException("Could not load file or resource: " + getResourcePath());
            return resourceAsStream;
        }
    }
}