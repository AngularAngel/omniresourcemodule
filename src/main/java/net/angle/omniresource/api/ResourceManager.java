package net.angle.omniresource.api;

import java.io.IOException;

/**
 *
 * @author angle
 */
public interface ResourceManager {
    public <T extends ResourceType> T addResourceType(T resourceType);
    
    public <T extends Resource> T addResource(T resource);
    
    public boolean resourceIsUsed(Resource resource);
    
    public Resource loadResource(String name, Object user) throws IOException;
    
    public Resource loadResource(String name, Object user, Class clazz) throws IOException;
    
    public <T extends Resource> T loadResource(ResourceType<T> type, String name, Object user) throws IOException;
    
    public <T extends Resource> T loadResource(ResourceType<T> type, String name, Object user, Class clazz) throws IOException;
    
        
    public void unloadResource(String name, Object user);
    
    public <T> T load(ResourceType<? extends Resource<T>> type, String name, Object user) throws IOException;
}