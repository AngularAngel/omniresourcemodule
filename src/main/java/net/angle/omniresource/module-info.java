
module omni.resource {
    requires static lombok;
    requires java.logging;
    requires omni.module;
    requires trivial.json;
    exports net.angle.omniresource.api;
    exports net.angle.omniresource.impl;
}