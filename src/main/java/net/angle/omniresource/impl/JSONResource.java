/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniresource.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;
import pixelguys.json.JsonObject;
import pixelguys.json.JsonParser;

/**
 *
 * @author angle
 */
public class JSONResource extends AbstractResource<JsonObject> {
    private static final String JSON_PATH = "json/";
    public static final ResourceType<Resource<JsonObject>> TYPE = new BasicResourceType<>(JSONResource::new, new String[]{"json"});
    private JsonObject json;
    
    protected JSONResource(String name) {
        super(name);
    }
    
    @Override
    public void load(ResourceManager resourceManager, Class clazz) throws IOException {
        json = JsonParser.parseObjectFromBufferedReader(new BufferedReader(new InputStreamReader(getInputStream(clazz))), null);
    }
    
    @Override
    public void unload(ResourceManager resourceManager) {
        json = null;
    }
    
    @Override
    public boolean isLoaded() {
        return json != null;
    }
    
    @Override
    public String getResourcePath() {
        return RESOURCE_PATH + JSON_PATH + getName();
    }
    
    @Override
    public JsonObject get() {
        return json;
    }

    @Override
    public ResourceType<Resource<JsonObject>> getType() {
        return TYPE;
    }
}