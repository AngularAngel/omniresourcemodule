package net.angle.omniresource.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.OmniResourceModule;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceType;
import pixelguys.json.JsonObject;

/**
 *
 * @author angle
 */
public class OmniResourceModuleImpl extends BasicOmniModule implements OmniResourceModule {
    
    Map<String, ResourceManager> sharedResourceManagers = new HashMap<>();

    public OmniResourceModuleImpl() {
        super(OmniResourceModule.class);
    }
    
    @Override
    public ResourceManager getPrivateResourceManager() {
        return new BasicResourceManager();
    }

    @Override
    public ResourceManager getSharedResourceManager(String name) {
        ResourceManager resourceManager = sharedResourceManagers.get(name);
        if (Objects.isNull(resourceManager)) {
            resourceManager = new BasicResourceManager();
            sharedResourceManagers.put(name, resourceManager);
        }
        return resourceManager;
    }

    @Override
    public ResourceType<Resource<JsonObject>> getJSONResourceType() {
        return JSONResource.TYPE;
    }
}