package net.angle.omniresource.impl;

import java.util.function.Function;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceType;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public class BasicResourceType<T extends Resource> implements ResourceType<T> {
    private final Function<String, T> constructor;
    private @Getter @Setter String[] extensions;
    
    @Override
    public T getResource(String name) {
        return constructor.apply(name);
    }
}