/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniresource.impl;

import lombok.Getter;
import net.angle.omniresource.api.Resource;

/**
 *
 * @author angle
 */
public abstract class AbstractResource<T> implements Resource<T> {
    public static final String RESOURCE_PATH = "resources/";
    
    private final @Getter String name;
    
    protected AbstractResource(String name) {
        this.name = name;
    }
}