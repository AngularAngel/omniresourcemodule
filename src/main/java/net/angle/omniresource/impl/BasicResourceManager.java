/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniresource.impl;

import net.angle.omniresource.api.Resource;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;

/**
 *
 * @author angle
 */
public class BasicResourceManager implements ResourceManager {
    
    private final List<ResourceType> resourceTypes = new ArrayList<>();
    private final Map<String, ResourceType> extensionMap = new HashMap<>();
    
    private final Map<Resource, Set<Object>> usageMap = new IdentityHashMap<>();
    private final Map<String, Resource> resourceMap = new HashMap<>();
    
    private static Map<Path, String> shaderSources;
    
    @Override
    public <T extends ResourceType> T addResourceType(T resourceType) {
        resourceTypes.add(resourceType);
        for (String extension : resourceType.getExtensions())
            extensionMap.put(extension, resourceType);
        return resourceType;
    }
    
    @Override
    public <T extends Resource> T addResource(T resource) {
        resourceMap.put(resource.getName(), resource);
        return resource;
    }
    
    @Override
    public boolean resourceIsUsed(Resource resource) {
        Set<Object> users = usageMap.get(resource);
        return (users != null && !users.isEmpty());
    }
    
    private <T extends Resource> T loadResource(T resource, Object user, Class clazz) throws IOException {
        Set<Object> users = usageMap.get(resource);
        if (users == null) {
            resource.load(this, clazz);
            users = Collections.newSetFromMap(new IdentityHashMap<>());
            usageMap.put(resource, users);
        }
        
        users.add(user);
        return resource;
    }
    
    @Override
    public <T extends Resource> T loadResource(ResourceType<T> type, String name, Object user) throws IOException {
        return loadResource(type, name, user, user.getClass());
    }

    @Override
    public <T extends Resource> T loadResource(ResourceType<T> type, String name, Object user, Class clazz) throws IOException {
        T resource = (T) resourceMap.get(name);
        if (resource == null) {
            resource = type.getResource(name);
            resourceMap.put(name, resource);
        }
        
        loadResource(resource, user, clazz);
        return resource;
    }
    
    private String getExtension(String name) {
        String[] pieces = name.split(".");
        return pieces[pieces.length - 1];
    }
    
    @Override
    public Resource loadResource(String name, Object user) throws IOException {
        return loadResource(extensionMap.get(getExtension(name)), name, user);
        
    }

    @Override
    public Resource loadResource(String name, Object user, Class clazz) throws IOException {
        return loadResource(extensionMap.get(getExtension(name)), name, user, clazz);
    }
    
    private void unloadResource(Resource resource, Object user) {
        Set<Object> users = usageMap.get(resource);
        if (users == null || !users.remove(user)) return;

        if (users.isEmpty()) {
            resource.unload(this);
            usageMap.remove(resource);
        }
    }
        
    @Override
    public void unloadResource(String name, Object user) {
        Resource resource = resourceMap.get(name);
        if (resource == null) return;

        unloadResource(resource, user);
        if (!resource.isLoaded()) resourceMap.remove(name);
    }
    
    @Override
    public <T> T load(ResourceType<? extends Resource<T>> type, String name, Object user) throws IOException {
        return loadResource(type, name, user).get();
    }
}